# from the tuto : http://outlace.com/Reinforcement-Learning-Part-1/
import numpy as np
from scipy import stats
import random
import matplotlib.pyplot as plt


def reward(prob):
    reward = 0
    for i in range(10):
        if random.random() < prob:
            reward += 1
    return reward


# greedy method to select best arm based on memory array (historical results)
def bestArm_old(a):
    bestArm = 0  # just default to 0
    bestMean = 0
    for u in a:
        avg = np.mean(a[np.where(a[:, 0] == u[0])][:, 1])  # calc mean reward for each action
        if bestMean < avg:
            bestMean = avg
            bestArm = u[0]
    return bestArm


def bestArm(a):
    return np.argmax(a)  # returns index of element with greatest value


n = 10  # 10 armed bandit promble
arms = np.random.rand(n)
eps = 0.1

# initialize memory array; has 1 row defaulted to random action index
# first create a tab [random(n+1),0]
# then reshape in a matrix : 1line/2colons
av_old = np.array([np.random.randint(0, (n + 1)), 0]).reshape(1, 2)  # av = action-value

av = np.ones(n)  # initialize action-value array
counts = np.zeros(n)  # stores counts of how many times we've taken a particular action


def main_old():
    plt.xlabel("Plays")
    plt.ylabel("Avg Reward")
    for i in range(1000):
        if random.random() > eps:  # greedy arm selection
            choice = bestArm_old(av_old)
            thisAV = np.array([[choice, reward(arms[choice])]])
            av = np.concatenate((av_old, thisAV), axis=0)
        else:  # random arm selection
            choice = np.where(arms == np.random.choice(arms))[0][0]
            thisAV = np.array([[choice, reward(arms[choice])]])  # choice, reward
            av = np.concatenate((av_old, thisAV), axis=0)  # add to our action-value memory array
        # calculate the percentage the correct arm is chosen (you can plot this instead of reward)
        percCorrect = 100 * (len(av[np.where(av[:, 0] == np.argmax(arms))]) / len(av))
        # calculate the mean reward
        runningMean = np.mean(av[:, 1])
        plt.scatter(i, runningMean)

    plt.show()


def main():
    plt.xlabel("Plays")
    plt.ylabel("Mean Reward")
    for i in range(1000):
        if random.random() > eps:
            choice = bestArm(av) # get the machine which the greater reward
            counts[choice] += 1 # increment its play row by one
            timesPlayed = counts[choice]
            rwd = reward(arms[choice])
            old_avg = av[choice]
            new_avg = old_avg + (1 / timesPlayed) * (rwd - old_avg)  # update running avg
            av[choice] = new_avg
        else:
            choice = np.where(arms == np.random.choice(arms))[0][0]  # randomly choose an arm (returns index)
            counts[choice] += 1
            timesPlayed = counts[choice]
            rwd = reward(arms[choice])
            old_avg = av[choice]
            new_avg = old_avg + (1 / timesPlayed) * (rwd - old_avg)  # update running avg
            av[choice] = new_avg
        # have to use np.average and supply the weights to get a weighted average
        runningMean = np.average(av, weights=np.array([counts[i] / np.sum(counts) for i in range(len(counts))]))
        plt.scatter(i, runningMean)
    plt.show()

if __name__ == "__main__":
    main()
