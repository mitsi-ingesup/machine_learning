## Supervised Learning
You have a lot of RIGHT examples and you use it to learn.
# Classification:
* You’d like software to examine individual customer accounts, and for each account decide if it has been hacked/compromised
* You have a set of data about breast cancer -> size and benign or malignant \n
	You want to know the if a new case is benign

# Regression: 
* Predict continuous valued output Exemple : House price
* You have a large inventory of identical items. You want to predict how many of these items will sell over the next 3 months

## Unsupervised Learning
You have a lot of data and you extract rule from it
 * Subject of news in http://news.google.com
